# Platform Builders Test API
# Built by Fernando Werneck - fernandowtb@hotmail.com - (31) 98594-8242

Teste de candidatura a vaga de desenvolvedor Java na Platform Builders, no qual foi desenvolvido um CRUD de Clientes.

## Requisitos

Para construir e/ou rodar esta aplicação, são necessários:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)
- [Docker](https://docs.docker.com/get-docker/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Executando o banco de dados da aplicação

Para executar o banco de dados da aplicação, deve-se iniciar o Docker Compose:

```shell
docker-compose up
```

Na primeira execução, este comando criará o container Docker com o Mysql 5.7, e uma base de dados chamada `platform_builders`.

## Executando a aplicaçao no ambiente local

Existem vários jeitos de executar a aplicação na sua máquina local. O primeiro deles é executar o método `main` da classe `com.fernandowerneck.clientapi.ClientApiApplication` pela sua IDE.

Outra maneira é usar o [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html):

```shell
mvn spring-boot:run
```

Na primeira execução da aplicação, a tabela client será criada no banco de dados, através do Liquibase.

## Considerações finais

Não criei nenhum tipo de teste, devido ao curto tempo disponível que tive, e considerando também que não fazia parte dos requisitos.

Devo reconhecer também que é possível fazer uma refatoração e melhorar o código, implementando um melhor tratamento de erros nos endpoints, por exemplo.

Nos requisitos também nada se falou sobre o formato do CPF, por isso, é possível salvá-lo usando formação ou não, respeitando o limite de 14 caracteres, cabendo então ao cliente consumidor da api fazer o tratamento destes dados.