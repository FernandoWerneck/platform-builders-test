package com.fernandowerneck.clientapi.rest;

import com.fernandowerneck.clientapi.model.Client;
import com.fernandowerneck.clientapi.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/client")
public class ClientController {

    @Autowired
    private ClientService service;

    @GetMapping
    public Page<Client> findAll(@RequestParam(defaultValue = "") String name,
                                @RequestParam(defaultValue = "") String cpf,
                                @RequestParam(defaultValue = "0") int page,
                                @RequestParam(defaultValue = "2") int size) {
        return service.findAllByDocumentAndNameContaining(cpf, name, page, size);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody Client client) {
        final Optional<Client> clientAlreadySaved = service.findByDocument(client.getDocument());
        if (clientAlreadySaved.isPresent()) {
            return ResponseEntity.badRequest().build();
        } else {
            return ResponseEntity.ok(service.save(client));
        }
    }

    @DeleteMapping(path ={"/{id}"})
    public ResponseEntity<?> delete(@PathVariable long id) {
        return service.findById(id)
                .map(record -> {
                    service.deleteById(id);
                    return ResponseEntity.ok().build();
                }).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping(value="/{id}")
    public ResponseEntity updatePost(@PathVariable("id") long id, @RequestBody Client client) {
        return service.findById(id)
                .map(record -> {
                    record.setDocument(client.getDocument());
                    record.setBirthDate(client.getBirthDate());
                    record.setName(client.getName());
                    Client updated = service.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }

    @PatchMapping(value="/{id}")
    public ResponseEntity updatePatch(@PathVariable("id") long id, @RequestBody Client client) {
        return service.findById(id)
                .map(record -> {
                    if (!StringUtils.isEmpty(client.getDocument())) {
                        record.setDocument(client.getDocument());
                    }
                    if (!StringUtils.isEmpty(client.getBirthDate())) {
                        record.setBirthDate(client.getBirthDate());
                    }
                    if (!StringUtils.isEmpty(client.getName())) {
                        record.setName(client.getName());
                    }
                    Client updated = service.save(record);
                    return ResponseEntity.ok().body(updated);
                }).orElse(ResponseEntity.notFound().build());
    }
}
