package com.fernandowerneck.clientapi.service;

import com.fernandowerneck.clientapi.model.Client;
import com.fernandowerneck.clientapi.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Page<Client> findAllByDocumentAndNameContaining(String document, String name, int page, int size) {

        final Page<Client> clients = clientRepository.findAllByDocumentContainingAndNameContaining(document, name, PageRequest.of(page, size));
        clients.stream().forEach(this::calculateAge);
        return clients;
    }

    private void calculateAge(Client client) {
        if (client != null && client.getBirthDate() != null) {
            client.setAge(Period.between(client.getBirthDate(), LocalDate.now()).getYears());
        }
    }

    public Client save(Client client) {
        client = clientRepository.save(client);
        calculateAge(client);
        return client;
    }

    public Optional<Client> findById(long id) {
        return clientRepository.findById(id);
    }

    public void deleteById(long id) {
        clientRepository.deleteById(id);
    }

    public Optional<Client> findByDocument(String document) {
        return clientRepository.findByDocument(document);
    }
}
