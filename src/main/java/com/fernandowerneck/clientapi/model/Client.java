package com.fernandowerneck.clientapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Entity
@Table(name = "client")
@Getter @Setter
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 240)
    private String name;
    @Size(max = 14, message = "Tamanho do campo cpf deve ser entre 0 e 14")
    @Column(length = 14, unique = true)
    @JsonProperty("cpf")
    private String document;
    @Column(name = "birth_date")
    @JsonProperty("birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd", iso = DateTimeFormat.ISO.DATE_TIME)
    private LocalDate birthDate;
    @Transient
    private int age;
}
