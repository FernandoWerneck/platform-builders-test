--liquibase formatted sql

--changeset fernando.barros:dbchangelog-01-create-client-table
CREATE TABLE `client` (
                          `id` bigint(20) NOT NULL AUTO_INCREMENT,
                          `birth_date` date DEFAULT NULL,
                          `document` varchar(14) DEFAULT NULL,
                          `name` varchar(240) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `uk_document` (`document`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1